import { useStore } from "../utils/Store";
import { LAYERS_DATA, ITEMS_DATA } from "./data";

const Layout = {
    Index: () => {
        const [store, setStore] = useStore();

        return [store.layout && store.layout.map((layer, index) => <Layout.GetElement key={index} data={layer} row={index} />)]
    },
    GetElement: ({ data, row, col }) => {
        if (!data) {
            return <div className="block-default"></div>;
        }
        switch (data.id) {
            case LAYERS_DATA.SINGLE: return <Layout.SingleCol data={data.data} row={row} />; break;
            case LAYERS_DATA.DUAL: return <Layout.DualCol data={data.data} row={row} />; break;
            case LAYERS_DATA.TRIPPLE: return <Layout.TrippleCol data={data.data} row={row} />; break;
            case ITEMS_DATA.LOGO: return <Layout.CampaignLogo data={data.data} row={row} col={col} />; break;
            case ITEMS_DATA.SUPPORT_BTN: return <Layout.Button data={data.data} row={row} col={col} />; break;
            default: return <div>fail</div>;
        }
    },
    SingleCol: ({ data, row }) => {
        const view = <div id={"row-" + row} className="layer-single item-layer drop-area-row">
            <div className={'hot-pan drop-area-row-col ' + (data[0] ? 'filled' : '')} id={'block-' + row + '-0'}>
                <Layout.GetElement data={data[0]} row={row} col={0} />
            </div>
        </div>
        return view;
    },
    DualCol: ({ data, row }) => {
        const view = <div id={"row-" + row} className="layer-dual item-layer drop-area-row">
            <div className={'hot-pan drop-area-row-col ' + (data[0] && data[0].id ? 'filled' : '')} id={'block-' + row + '-0'}>
                <Layout.GetElement data={data[0]} row={row} col={0} />
            </div>
            <div className={'hot-pan drop-area-row-col ' + (data[1] && data[1].id ? 'filled' : '')} id={'block-' + row + '-1'}>
                <Layout.GetElement data={data[1]} row={row} col={1} />
            </div>
        </div>
        return view;
    },
    TrippleCol: ({ data, row }) => {
        const view = <div id={"row-" + row} className="layer-tripple item-layer drop-area-row">
            <div className={'hot-pan drop-area-row-col ' + (data[0] && data[0].id ? 'filled' : '')} id={'block-' + row + '-0'}>
                <Layout.GetElement data={data[0]} row={row} col={0} />
            </div>
            <div className={'hot-pan drop-area-row-col ' + (data[1] && data[1].id ? 'filled' : '')} id={'block-' + row + '-1'}>
                <Layout.GetElement data={data[1]} row={row} col={1} />
            </div>
            <div className={'hot-pan drop-area-row-col ' + (data[2] && data[2].id ? 'filled' : '')} id={'block-' + row + '-2'}>
                <Layout.GetElement data={data[2]} row={row} col={2} />
            </div>
        </div>
        return view;
    },
    CampaignLogo: ({ data, row, col }) => {
        const [store, setStore] = useStore();

        function editItem() {
            setStore({ ...store, editBlock: { row, col, id: ITEMS_DATA.LOGO, data } })
        }
        return <div className="block-item block-filled" onClick={editItem}>
            <div className="block-item-logo">
                {data && data.image ? <img src={data.image} /> : ''}
            </div>
        </div>
    },
    Button: ({ data }) => {
        return <div className="block-item block-filled">
            <div className="block-item-btn">
                <button>{data.label}</button>
            </div>
        </div>
    }
}

export default Layout;