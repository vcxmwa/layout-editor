import { useEffect, useState } from 'react';
import './Home.scss';
import { useStore } from '../utils/Store';
import Layout from './Layout';
import EditBlock from './EditBlock';
import { LAYER_LIST, ITEM_LIST } from './data';

var React = require('react');
var dragula = require('react-dragula');


const Home = {
    Index: () => {
        const [store, setStore] = useStore();
        const [newLayer, setNewLayer] = useState({ position: null, type: null, oldIndex: null });
        const [blockItem, setBlockitem] = useState({ row: null, col: null, type: null });
        const canvasRef = React.useRef();
        const [hotPanListener, setHotPanListener] = useState();

        useEffect(() => {
            const canvasContainer = canvasRef.current;
            const pickerlayerContainer = canvasContainer.querySelector('#picker-dom #layout-list');
            const dropContainer = canvasContainer.querySelector('#droparea-dom');

            dragula([pickerlayerContainer, dropContainer], {
                copy: function (el, source) {
                    return source === pickerlayerContainer
                },
                removeOnSpill: false,
                revertOnSpill: true,
                accepts: function (el, target) {
                    return target === dropContainer
                }
            }).on('drag', el => {
                if (el.classList.contains('drop-area-row')) {
                    el.classList.add('moving');
                }
            }).on('drop', (el, target) => {
                const elType = el.getAttribute('id');

                let newRowIndex = null;
                let oldIndex = null;

                const elList = dropContainer.querySelectorAll('.item-layer');

                for (let i = 0; i < elList.length; i++) {
                    if (elList[i]) {
                        const thisEl = elList[i];
                        if (elType === thisEl.getAttribute('id') && thisEl.getAttribute('id')) {
                            newRowIndex = i;
                        }

                        if (thisEl.classList.contains('moving')) {
                            oldIndex = +elType.replace('row-', '');
                            break;
                        }
                    }
                }

                if (newRowIndex > -1) {
                    const layoutList = [...store.layout];
                    layoutList.splice(newRowIndex, 0, { id: elType, data: [] });
                    setNewLayer({ position: newRowIndex, type: elType, oldIndex: oldIndex });
                }
            }).on('dragend', (el) => {
                if (!el.classList.contains('moving')) { el.remove(); }
                el.classList.remove('moving');
            });
        }, []);

        // listen to new layer addition
        useEffect(() => {
            const layoutList = [...store.layout];
            let data = []; // preset contents for a block
            if (newLayer.oldIndex !== null && newLayer.oldIndex !== undefined) {
                data = layoutList[newLayer.oldIndex].data;

                newLayer.type = data.id;

                layoutList.splice(newLayer.oldIndex, 1);
            }
            if (newLayer.type) {
                layoutList.splice(newLayer.position, 0, { id: newLayer.type, data: data });

                setStore({ layout: layoutList });
                setNewLayer({ position: null, type: null, move: false });
                setTimeout(() => resetHotPans());
            }

        }, [newLayer.type]);


        // listen to new block addition
        useEffect(() => {
            if (blockItem.type) {
                const layoutList = [...store.layout];

                layoutList[blockItem.row].data[blockItem.col] = {
                    id: blockItem.type,
                    data: EditBlock.getPreset(blockItem.type)
                };
                setStore({ layout: layoutList });
                setBlockitem({ row: null, type: null, col: null });
            }

        }, [blockItem]);

        function resetHotPans() {
            const canvasContainer = canvasRef.current;
            const pickerItemsContainer = canvasContainer.querySelector('#picker-dom #layout-items-list');
            let hotPan = canvasContainer.querySelectorAll('#droparea-dom .hot-pan');

            if (hotPanListener) {
                hotPanListener.destroy();
            }

            const listner = dragula([pickerItemsContainer, ...hotPan], {
                removeOnSpill: false,
                revertOnSpill: true,
                copy: function (el, source) {
                    return source === pickerItemsContainer
                },
                accepts: function (el, target) {
                    return !target.classList.contains('filled') && !el.classList.contains('block-filled');
                },
                moves: (el, target, handle) => {
                    return handle.classList.contains('drop-item');
                }
            }).on('drag', (el) => {
                el.classList.add('move');
            }).on('drop', (el, target) => {
                if (target) {
                    const pos = target.id.split('-');
                    setBlockitem({ row: pos[1], col: pos[2], type: el.id });
                }
            }).on('dragend', (el) => {
                if (!el.classList.contains('block-item')) {
                    el.remove();
                }
            });
            setHotPanListener(listner)
        }

        const view =
            <div ref={canvasRef} className="canvas">
                <Home.Canvas />
                <Home.Picker />
                <div style={{ width: '200px' }}>
                    {store.editBlock && store.editBlock.id ? <EditBlock.Index /> : ''}
                    <div style={{ marginTop: '50px' }}>{JSON.stringify(store.layout, null, 3)}</div>
                </div>
            </div>;
        return view;
    },
    Canvas: () => {
        return <div id="droparea-dom" className="droparea-dom">
            <Layout.Index />
        </div>;
    },
    Picker: () => {
        const layoutList = LAYER_LIST;
        const pickUpList = ITEM_LIST;

        const list = <div id="picker-dom">
            <div className="section-name">Layouts</div>
            <div id="layout-list" className="picker-dom-layout-list">
                {layoutList.map(item => <div key={item.id} id={item.id} className={'drop-item item-layer '}><span className
                    ="label">{item.name}</span></div>)}
            </div>
            <div className="section-name">Insert</div>
            <div id="layout-items-list" className="picker-dom-layout-item-list">
                {pickUpList.map(item => <div key={item.id} id={item.id} className={'drop-item item-block'}><span className
                    ="label">{item.name}</span></div>)}
            </div>
        </div>;

        return list
    }
};

export default Home;