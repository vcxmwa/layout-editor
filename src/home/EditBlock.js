import { useState, useEffect } from "react";
import { useStore } from "../utils/Store";

const { ITEMS_DATA } = require("./data");

const EditBlock = {
    Index: ({ type }) => {
        const [store, setStore] = useStore();

        switch (store.editBlock.id) {
            case ITEMS_DATA.LOGO: return <EditBlock.CampaignLogo />; break;
            case ITEMS_DATA.SUPPORT_BTN: return <EditBlock.Button />; break;
            default: return <div>fail</div>;
        }

    },
    getPreset: (type) => {
        let data = {};
        switch (type) {
            case ITEMS_DATA.LOGO:
                data = { image: 'https://th.bing.com/th/id/Rf9b5bc2fc93bb0800ac871115ac95f8d?rik=imzK0xsJn3MlhA&pid=ImgRaw' };
                break;
            case ITEMS_DATA.SUPPORT_BTN:
                data = { label: 'Support now' };
                break;
        }
        return data;
    },
    CampaignLogo: () => {
        const [store, setStore] = useStore();

        const [formControl, setFormControl] = useState('');

        useEffect(() => {
            setFormControl(store.editBlock.data.image);
        }, [store.editBlock.data]);

        function formChanges(event) {
            setFormControl(event.target.value);
        }

        function save() {
            const layout = store.layout;
            layout[store.editBlock.row].data[store.editBlock.col] = {
                id: ITEMS_DATA.LOGO,
                data: {
                    image: formControl
                }
            }

            setStore({ ...store, layout, editBlock: null })
        }

        const view = <div>
            Img url: <input type="text" onChange={formChanges.bind(this)} value={formControl} />
            <button onClick={save}>Submit</button>
        </div>

        return view;
    }
}

export default EditBlock;