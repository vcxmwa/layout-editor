
export const LAYERS_DATA = {
    SINGLE: 'layer-single',
    DUAL: 'layer-split',
    TRIPPLE: 'layer-tripple'
};

export const ITEMS_DATA = {
    LOGO: 'oc-logo',
    SUPPORT_BTN: 'support-btn',
    ACTION_FEED: 'action-feed'
};

export const LAYER_LIST = [
    {
        id: LAYERS_DATA.SINGLE,
        name: '1 column',
        className: 'layer-single'
    },
    {
        id: LAYERS_DATA.DUAL,
        name: '2 columns',
        className: 'layer-split'
    },
    {
        id: LAYERS_DATA.TRIPPLE,
        name: '3 columns',
        className: 'layer-tripple'
    }
];

export const ITEM_LIST = [
    {
        id: ITEMS_DATA.LOGO,
        name: 'Campaign logo',
        className: 'block-logo'
    },
    {
        id: ITEMS_DATA.SUPPORT_BTN,
        name: 'Support Button',
        className: 'block-button'
    },
    {
        id: ITEMS_DATA.ACTION_FEED,
        name: 'Action-feed',
        className: 'block-feed'
    }
]