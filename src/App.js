import logo from './logo.svg';
import './App.css';
import Home from './home/Home';

function App() {
  return (
    <div className="App">
      <Home.Index />
    </div>
  );
}

export default App;
